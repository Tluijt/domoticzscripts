#!/usr/bin/python
import sys
import json
from urllib.request import urlopen, Request
import urllib.parse
import re
import time
import datetime
import urllib

def open_port():
    pass

def close_port():
    pass


class Domoticz():

    def __init__(self, url):

        self.baseurl = url

    def __execute__(self, url):

        request = Request(url)
        response = urlopen(request)
        return response

    def set_device_on(self, xid):
        """
        Get the Domoticz device information.
        """
        url = "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=On" % (self.baseurl, xid)
        data = json.load(self.__execute__(url))
        return data

    def set_device_off(self, xid):
        """
        Get the Domoticz device information.
        """
        url = "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=Off" % (self.baseurl, xid)
        data = json.load(self.__execute__(url))
        return data

    def single_device_info(self, xid):
        url = "%s/json.htm?type=devices&rid=%s" % (self.baseurl, xid)
        device_info = json.load(self.__execute__(url))
        return device_info

#Get movement senor information
def get_devices(url, device_id):
    device_info = Domoticz(url).single_device_info(device_id)
    return device_info

xid = *domoticz idx here*
domoticzurl = "DOMOTICZ_URL HERE"

#Blink when the movement sensor has indicated movement if not print no movement.
def flashing(url, device_id):
    time.sleep(10)
    if (get_devices(domoticzurl, xid)['result'][0]['Data'] == "On"):
        stop =  time.time()+300
        while time.time() < stop:
            on = Domoticz(url).set_device_on(device_id)
            time.sleep(2)
            off = Domoticz(url).set_device_off(device_id)
            time.sleep(2)
    else:
        print("No Movement...")


domoticz_idx = *domoticz idx light that should blink here*

flash = flashing(domoticzurl, domoticz_idx)
