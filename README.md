# DomoticzScripts

Useful scripts for Domoticz


## Blinking light

Original script from: https://github.com/scns/Domoticz-blinking-light  
Stole it and converted it to python3  
Also added an extra step to check for movement before starting to blink.

## Timed light 
Script for turning the light on, based on movement from a sensor.
