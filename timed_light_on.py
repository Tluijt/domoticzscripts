#!/usr/bin/python
import sys
import json
from urllib.request import urlopen, Request
import urllib.parse
import re
import time
import datetime
import urllib

def open_port():
    pass

def close_port():
    pass


class Domoticz():

    def __init__(self, url):

        self.baseurl = url

    def __execute__(self, url):

        request = Request(url)
        response = urlopen(request)
        return response

    def set_device_on(self, xid):
        """
        Get the Domoticz device information.
        """
        url = "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=On" % (self.baseurl, xid)
        data = json.load(self.__execute__(url))
        return data

    def set_device_off(self, xid):
        """
        Get the Domoticz device information.
        """
        url = "%s/json.htm?type=command&param=switchlight&idx=%s&switchcmd=Off" % (self.baseurl, xid)
        data = json.load(self.__execute__(url))
        return data


current_time = datetime.datetime.now()

#Turn on the light for a specific time in a certain time window
def timed_light_on(url, device_id):
    if (current_time.hour < 6) or (current_time.hour > 16):
        stop = time.time()+200 # Change accordingly
        while time.time() < stop:
            on = Domoticz(url).set_device_on(device_id)
            time.sleep(200)
            off = Domoticz(url).set_device_off(device_id)
            time.sleep(2)
    else:
        print("not the right time!")


domoticzurl = "DOMOTICZ_URL HERE"
domoticzdevice_id = *DOMOTICZ_IDX from the light you want to turn on here*

light_on = timed_light_on(domoticzurl, domoticzdevice_id)
